package com.example.models

import kotlinx.serialization.Serializable

@Serializable
data class Pelicules(
    val id: String,
    val titol: String,
    val any: String,
    val genere: String,
    val directior: String)

val peliculesStorage = mutableListOf<Pelicules>()

