package com.example.models

import kotlinx.serialization.Serializable


@Serializable
data class Comentari(val id: String, val idPelicula: String, val cometari: String, val data: String )

val comentarisStorage = mutableListOf<Comentari>()
