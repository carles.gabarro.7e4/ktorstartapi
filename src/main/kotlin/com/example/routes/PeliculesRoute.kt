package com.example.routes

import com.example.models.Comentari
import com.example.models.Pelicules
import com.example.models.comentarisStorage
import com.example.models.peliculesStorage
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

    fun Route.peliculesRouting(){
        route("/pelicules"){

            get("/all") {
                if (peliculesStorage.isNotEmpty()) {
                    call.respond(peliculesStorage)
                } else {
                    call.respondText("No movies found", status = HttpStatusCode.OK)
                }
            }

            get("/{id?}"){
                val id = call.parameters["id"] ?: return@get call.respondText(
                    "Missing id",
                    status = HttpStatusCode.BadRequest
                )
                val pelicula =
                    peliculesStorage.find { it.id == id } ?: return@get call.respondText(
                        "No movie with id $id",
                        status = HttpStatusCode.NotFound
                    )
                call.respond(pelicula)
            }

            get("/{id?}/comments"){
                val id = call.parameters["id"] ?: return@get call.respondText(
                    "Missing id",
                    status = HttpStatusCode.BadRequest
                )
                val comentaris =
                    comentarisStorage.find { it.idPelicula == id } ?: return@get call.respondText(
                        "No comentaries with id $id",
                        status = HttpStatusCode.NotFound
                    )
                call.respond(comentaris)
            }

            post("/add") {
                val pelicula = call.receive<Pelicules>()
                peliculesStorage.add(pelicula)
                call.respondText("Movie stored correctly", status = HttpStatusCode.Created)
            }

            post("/{id?}/add"){
                val id = call.parameters["id"] ?: return@post call.respondText(
                    "Missing id",
                    status = HttpStatusCode.BadRequest
                )
                val comentari = call.receive<Comentari>()
                comentarisStorage.add(comentari).and(comentari.idPelicula==id)
                call.respondText("Movie stored correctly", status = HttpStatusCode.Created)
            }

            put("/update/{id?}") {}

            delete("/delete/{id?}") {
                val id = call.parameters["id"] ?: return@delete call.respond(HttpStatusCode.BadRequest)
                if (peliculesStorage.removeIf { it.id == id }) {
                    call.respondText("Movie removed correctly", status = HttpStatusCode.Accepted)
                } else {
                    call.respondText("Not Found", status = HttpStatusCode.NotFound)
                }
            }
        }
    }
